from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
import requests
import json

from google.oauth2 import id_token
from google.auth.transport import requests as grequests

def index(req):
    if req.method == 'POST' and req.is_ajax():
        data = json.loads(req.body)
        if (data['type'] == 'postFavorite'):
            newCount = int(data['favCount']) + 1
            req.session['favCount'] = str(newCount)
            return JsonResponse({'favCount' : str(newCount)})
        elif (data['type'] == 'postKeyword'):
            keyword = data['keyword']
            r = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + keyword)
            res = r.json()
            req.session['favCount'] = '0'
            return JsonResponse({'items' : res['items'], 'keyword': keyword, 'favCount': 0})
        else:
            return JsonResponse({'error': 'Unknown type'})

    else:
        try:
            userid = req.session['userid']
            r = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
            res = r.json()
            req.session['favCount'] = '0'
            return render(req, 'books.html', {'books': res['items'], 'favCount': 0, 'keyword': 'quilting'})
        except KeyError:
            return HttpResponseRedirect('login')

def login(req):
    if req.method == 'POST' and req.is_ajax():
        data = json.loads(req.body)
        if (data['type'] == 'googleAuth'):
            try:
                # Specify the CLIENT_ID of the app that accesses the backend:
                idinfo = id_token.verify_oauth2_token(data['idToken'], grequests.Request(), '836085326501-hulpvrrhnagkha9fp1hljcj7vvs531v0.apps.googleusercontent.com')

                if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                    raise ValueError('Wrong issuer.')

                # ID token is valid. Get the user's Google Account ID from the decoded token.
                userid = idinfo['sub']
                req.session['userid'] = userid
                req.session['name'] = data['name']
                req.session['imgUrl'] = data['imgUrl']
                req.session['email'] = data['email']
                return JsonResponse({'url': 'http://localhost:8000/books/'})
            except ValueError:
                return JsonResponse({'error': 'Invalid Google Auth token'})
        elif (data['type'] == 'googleLogout'):
            del req.session['userid']
            del req.session['name']
            del req.session['imgUrl']
            del req.session['email']
            return JsonResponse({'url': 'http://localhost:8000/books/login'})
        else:
            return JsonResponse({'error': 'Unknown type'})
    else:
        return render(req, 'login.html')
