from django.test import TestCase
import json
import requests

class BooksUnitTest(TestCase):
    def test_uses_correct_template(self):
        res = self.client.get('/books')
        self.assertTemplateUsed(res, 'books.html')

    def test_response_status_code(self):
        res = self.client.get('/books')
        self.assertEqual(res.status_code, 200)

    def test_ajax_post(self):
        res = self.client.post('/books', {'favCount': '0', 'type': 'postFavorite'}, content_type='application/json', **{ 'HTTP_X_REQUESTED_WITH':'XMLHttpRequest' })
        self.assertEqual(res.status_code, 200)
        data = json.loads(res.content)
        self.assertEqual(data['favCount'], '1')

    def test_ajax_keyword_post(self):
        res = self.client.post('/books', {'keyword': 'test', 'type': 'postKeyword'}, content_type='application/json', **{ 'HTTP_X_REQUESTED_WITH':'XMLHttpRequest' })
        self.assertEqual(res.status_code, 200)
        data = json.loads(res.content)

        r = requests.get('https://www.googleapis.com/books/v1/volumes?q=test')
        r = r.json()

        self.assertEqual(data['favCount'], 0)
        self.assertEqual(data['keyword'], 'test')
        self.assertEqual(data['items'], r['items'])
