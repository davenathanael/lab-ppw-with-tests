const debug = false
const dev = false

async function onSignIn(googleUser) {
  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value

  const profile = googleUser.getBasicProfile();
  const idToken = googleUser.getAuthResponse().id_token;

  const name = profile.getName()
  const imgUrl = profile.getImageUrl()
  const email = profile.getEmail()

  console.log("Logged in as:");
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

  const url = dev ? 'http://localhost:8000/books/login' : 'http://lab-with-tests.herokuapp.com/books/login'
  const opt = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({
      idToken,
      name,
      imgUrl,
      email,
      type: 'googleAuth',
    })
  }
  const json = await (await fetch(url, opt)).json()

  if (!json.error) window.location = json.url
  else console.error(err)
}

function signOut() {
    const auth2 = gapi.auth2.getAuthInstance()

    auth2.signOut().then(function () {
      console.log('User signed out.');
      const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
      const idToken = document.getElementById('googleuserid').innerText

      const url = dev ? 'http://localhost:8000/books/login' : 'http://lab-with-tests.herokuapp.com/books/login'
      const opt = {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-CSRFToken': token,
          'X-Requested-With': 'XMLHttpRequest',
        },
        body: JSON.stringify({
          idToken,
          type: 'googleLogout',
        })
      }
      fetch(url, opt)
      .then(res => res.json())
      .then(json => {
        console.log(json);
        if (json.url) window.location = json.url
      })
    })
  }

function onLoad() {
  gapi.load('auth2', function() {
    gapi.auth2.init();
  });
}


const postFavorite = async (event, title) => {
  event.preventDefault(true)
  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const favCount = document.getElementById('favCount').innerText
  if (debug) console.table([token, favCount])

  const url = dev ? 'http://localhost:8000/books/' : 'http://lab-with-tests.herokuapp.com/books/'
  const opt = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({
      title,
      favCount,
      type: 'postFavorite'
    })
  }

  const json = await (await fetch(url, opt)).json()
  const el = event.target
  document.getElementById('favCount').innerText = json.favCount
  el.classList = 'btn btn-outline-success'
  el.setAttribute('disabled', true)
  el.innerText = 'Favorited'
}

const postKeyword = async (event) => {
  event.preventDefault(true)
  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const keyword = document.getElementById('keywordField').value

  const url = dev ? 'http://localhost:8000/books/' : 'http://lab-with-tests.herokuapp.com/books/'
  const opt = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({
      keyword,
      type: 'postKeyword'
    })
  }

  const json = await (await fetch(url, opt)).json()
  if (debug) console.log(json)


  // DOM Manipulation
  document.getElementById('keyword').innerText = json.keyword
  document.getElementById('favCount').innerText = json.favCount

  document.getElementsByTagName('table')[0].remove()

  const table = document.createElement('table')
  table.classList = 'table table-striped'

  const thead = document.createElement('thead')
  thead.classList = 'thead-dark'

  const theadTr = document.createElement('tr')
  const thTexts = ['No', 'Title', 'Publisher', '']

  thTexts.forEach(text => {
    let th = document.createElement('th')
    th.innerText = text
    th.setAttribute('scope', 'col')
    theadTr.appendChild(th)
  })

  thead.appendChild(theadTr)
  table.appendChild(thead)

  const tbody = document.createElement('tbody')
  let counter = 1
  json.items.forEach(item => {
    let tr = document.createElement('tr')

    let th = document.createElement('th')
    th.setAttribute('scope', 'col')
    th.innerText = counter
    counter++

    tr.appendChild(th)

    let { title, publisher } = item.volumeInfo
    let cols = [title, publisher]

    cols.forEach(col => {
      let td = document.createElement('td')
      td.innerText = col
      tr.appendChild(td)
    })

    let tdButton = document.createElement('td')

    let button = document.createElement('button')
    button.classList = 'btn btn-success'
    button.setAttribute('onclick', `postFavorite(event, "${item.volumeInfo.title}")`)
    button.innerText = 'Add to favorite'
    tdButton.appendChild(button)
    tr.appendChild(tdButton)

    tbody.appendChild(tr)
  })

  table.appendChild(tbody)

  const card = document.getElementsByClassName('card')[0]
  card.appendChild(table)
}
