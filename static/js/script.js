$(document).ready(() => {
  $( ".accordion" ).accordion({ active: false, collapsible: true, heightStyle: 'content' })
  setTimeout(() => {
    $('.loading').css('height', '0')
    $('header').css('display', 'flex')
  }, 2500)
})


$('#theme-changer').click(() => {
  $('body').toggleClass('theme-secondary')
})
