const getSubscribers = async () => {
  const dev = true

  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const url = dev ? 'http://localhost:8000/subscribe/subscribers' : 'http://lab-with-tests.herokuapp.com/subscribe/subscribers'
  const opt = {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
  }

  const json = await (await fetch(url, opt)).json()

  console.log(json.subscribers)

  document.getElementById('subscribers').remove()
  const subscribersList = document.createElement('section')
  subscribersList.setAttribute('id', 'subscribers')

  json.subscribers.forEach(sub => {
    const entry = document.createElement('div')
    entry.classList.add(...['card', 'card-body', 'my-3'])
    const text = document.createElement('p')
    text.innerText = `${sub.email} ${sub.name}`
    const button = document.createElement('button')
    button.innerText = 'Unsubscribe'
    button.setAttribute('type', 'button')
    button.setAttribute('data-toggle', 'collapse')
    button.setAttribute('data-target', '#collapse' + sub.id)
    button.classList.add(...['btn', 'btn-danger', 'ml-2'])

    text.appendChild(button)
    entry.appendChild(text)

    const collapsable = document.createElement('div')
    collapsable.classList.add('collapse')
    collapsable.setAttribute('id', 'collapse' + sub.id)

    const card = document.createElement('div')
    card.classList.add(...['card', 'card-body'])

    const passwordField = document.createElement('input')
    passwordField.setAttribute('id', 'passwordFor' + sub.id)
    passwordField.setAttribute('type', 'password')
    passwordField.classList.add(...['form-control', 'mb-3'])



    const unsubButton = document.createElement('button')
    unsubButton.innerText = 'Submit'
    unsubButton.setAttribute('onclick', `unsubscribe(event, '${sub.email}', ${sub.id})`)
    unsubButton.classList.add(...['btn', 'btn-danger'])

    card.appendChild(passwordField)
    card.appendChild(unsubButton)

    collapsable.appendChild(card)
    entry.appendChild(collapsable)
    subscribersList.appendChild(entry)
  })

  document.getElementsByClassName('container')[0].appendChild(subscribersList)
}

const checkEmail = async () => {
  const dev = true

  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const url = dev ? 'http://localhost:8000/subscribe/' : 'http://lab-with-tests.herokuapp.com/subscribe/'
  const email = document.getElementById('emailfield').value
  const opt = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({
      email,
      type: 'checkEmailRegistered'
    })
  }

  const json = await (await fetch(url, opt)).json()
  if (dev) console.log('response', json)
  if (json.registered == false) {
    showSuccess('Email is available to use!')
    document.getElementById('submitButton').removeAttribute('disabled')
  } else {
    showError('Email already registered')
  }
}

const postSubscription = async (event) => {
  event.preventDefault()
  const dev = true
  const emailField = document.getElementById('emailfield')
  const nameField = document.getElementById('namefield')
  const passwordField = document.getElementById('passwordfield')

  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const url = dev ? 'http://localhost:8000/subscribe/' : 'http://lab-with-tests.herokuapp.com/subscribe/'

  const opt = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({
      email: emailField.value,
      name: nameField.value,
      password: passwordField.value,
      type: 'postSubscription'
    })
  }

  const json = await (await fetch(url, opt)).json()

  const info = document.getElementById('info')

  show(info)
  info.innerText = json.message
  emailField.value = ''
  nameField.value = ''
  passwordField.value = ''
  document.getElementById('submitButton').setAttribute('disabled', true)
  getSubscribers()
}

const unsubscribe = async (event, email, id) => {
  event.preventDefault()
  const dev = true
  const passwordField = document.getElementById('passwordFor'+id)

  const token = document.getElementsByName('csrfmiddlewaretoken')[0].value
  const url = dev ? 'http://localhost:8000/subscribe/unsubscribe' : 'http://lab-with-tests.herokuapp.com/subscribe/unsubscribe'

  const opt = {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-CSRFToken': token,
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify({
      email,
      password: passwordField.value,
      type: 'unsubscribe'
    })
  }

  const json = await (await fetch(url, opt)).json()

  if (json.error) {
    alert(json.message)
  } else {
    getSubscribers()
  }
}

const validateEmail = email => {
  const re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
  return re.test(email)
}

const hide = el => el.style.display = 'none'
const show = el => el.style.display = 'block'

const showError = message => {
  hide(document.getElementsByClassName('validFeedback')[0])
  show(document.getElementsByClassName('invalidFeedback')[0])
  document.getElementsByClassName('invalidFeedback')[0].innerText = message
}

const showSuccess = message => {
  hide(document.getElementsByClassName('invalidFeedback')[0])
  show(document.getElementsByClassName('validFeedback')[0])
  document.getElementsByClassName('validFeedback')[0].innerText = message
}



(() => {
  document.getElementById('emailfield').addEventListener('input', (e) => {
    console.log(e.target.value)
    const button = document.getElementById('submitButton')
    console.log('disabled:', button.getAttribute('disabled'))
    if (button.getAttribute('disabled') == false || button.getAttribute('disabled') == null)
      button.setAttribute('disabled', true)
    if (validateEmail(e.target.value) === false) {
      showError('Please use a valid email address!')
    } else {
      showSuccess('Checking email...')
      checkEmail()
    }
  }, false)
  getSubscribers()
})()
