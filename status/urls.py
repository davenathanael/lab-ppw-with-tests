from django.contrib import admin
from django.urls import path
from status import views

urlpatterns = [
    path('', views.landing, name='landing'),
    path('profile',views.profile, name='profile')
]
