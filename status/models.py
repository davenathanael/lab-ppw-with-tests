from django.db import models

# Create your models here.
class Status(models.Model):
    text = models.TextField(default='')
    time = models.DateTimeField(null=True)
