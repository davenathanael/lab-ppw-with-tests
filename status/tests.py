from django.test import TestCase
from django.urls import resolve
from django.http import HttpRequest
from .views import landing
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

class LandingPageTest(TestCase):
    def test_uses_landing_page_template(self):
        res = self.client.get('/')
        self.assertTemplateUsed(res, 'landing.html')

    def test_index_response_status_code(self):
        res = self.client.get('/')
        self.assertEqual(res.status_code, 200)

    def test_can_save_a_POST_request(self):
        res = self.client.post('/', data={'status_text': 'Status baru'})

        self.assertEqual(Status.objects.count(), 1)
        new_status = Status.objects.first()
        self.assertEqual(new_status.text, 'Status baru')

    def test_redirects_after_POST(self):
        res = self.client.post('/', data={'status_text': 'Status baru'})

        self.assertEqual(res.status_code, 302)
        self.assertEqual(res['location'], '/')

    def test_only_save_while_necessary(self):
        self.client.get('/')
        self.assertEqual(Status.objects.count(), 0)

    def test_display_all_entries(self):
        Status.objects.create(text='status 1')
        Status.objects.create(text='status 2')

        res = self.client.get('/')

        self.assertIn('status 1', res.content.decode())
        self.assertIn('status 2', res.content.decode())

class ProfilePageTest(TestCase):
    def test_page_status_code(self):
        res = self.client.get('/profile')
        self.assertEqual(res.status_code, 200)

    def test_page_using_correct_template(self):
        res = self.client.get('/profile')
        self.assertTemplateUsed(res, 'profile.html')

class StatusModelTest(TestCase):
    def test_saving_and_retrieving_status(self):
        first_status = Status()
        first_status.text = 'Status pertama'
        first_status.save()

        second_status = Status()
        second_status.text = 'Status kedua'
        second_status.save()

        saved_items = Status.objects.all()
        self.assertEqual(saved_items.count(), 2)
        self.assertEqual(saved_items[0].text, 'Status pertama')
        self.assertEqual(saved_items[1].text, 'Status kedua')

class FunctionalTest(unittest.TestCase):
    # url = 'http://localhost:8000'
    url = 'http://lab-with-tests.herokuapp.com/'

    def setUp(self):
        options = Options()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options=options)

    def tearDown(self):
        self.browser.quit()

    def check_for_entry_in_page(self, text):
        entries = self.browser.find_elements_by_class_name('status')
        self.assertTrue(any([text in entry.text for entry in entries]))

    def find_by_css(self, selector):
        return self.browser.find_element_by_css_selector(selector)

    def test_usage(self):
        self.browser.get(self.url)
        self.assertIn('Landing Page', self.browser.title)

        title_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Hello, Apa kabar?', title_text)

        inputbox = self.browser.find_element_by_id('input')
        self.assertEqual(inputbox.get_attribute('placeholder'), 'Status hari ini')

        inputbox.send_keys('Coba Coba')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(5)
        self.check_for_entry_in_page('Coba Coba')

        inputbox = self.browser.find_element_by_id('input')
        inputbox.send_keys('Coba Coba lagi')
        inputbox.send_keys(Keys.ENTER)
        time.sleep(5)

        self.check_for_entry_in_page('Coba Coba')
        self.check_for_entry_in_page('Coba Coba lagi')

    # def test_form_after_h1(self):
    #     self.browser.get(self.url)
    #
    #     formElement = self.find_by_css('h1 + form')
    #     self.assertTrue(formElement.tag_name == 'form')

    def test_submit_button_after_input_field(self):
        self.browser.get(self.url)

        submitButton = self.find_by_css('input#input + input[type="submit"]')
        self.assertTrue(submitButton.tag_name == 'input')
        self.assertTrue(submitButton.get_attribute('type') == 'submit')

    def test_font_used(self):
        self.browser.get(self.url)

        inputElement = self.find_by_css('input#input')
        h1 = self.find_by_css('h1')
        p = self.find_by_css('p')

        self.assertTrue(inputElement.value_of_css_property('font-family') == 'Poppins')
        self.assertTrue(h1.value_of_css_property('font-family') == 'Poppins')
        self.assertTrue(p.value_of_css_property('font-family') == 'Poppins')

    def test_status_border(self):
        self.browser.get(self.url)

        statusElement = self.find_by_css('.status')

        self.assertTrue(statusElement.value_of_css_property('border-left') == '4px solid rgb(38, 110, 255)')
