from django.shortcuts import render, redirect
from .models import Status
from datetime import datetime

def profile(req):
    return render(req, 'profile.html')

def landing(req):
    if req.method == 'POST':
        new_status = req.POST['status_text']
        Status.objects.create(text = new_status, time=datetime.now())
        return redirect('/')

    entries = Status.objects.all()
    return render(req, 'landing.html', {'entries': entries})
