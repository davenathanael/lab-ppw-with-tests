from django.shortcuts import render
from django.http import JsonResponse
from .models import Subscription
import json

def index(req):
    if req.method == 'POST' and req.is_ajax():
        data = json.loads(req.body)
        if (data['type'] == 'checkEmailRegistered'):
            isRegistered = Subscription.objects.all().filter(email=data['email']).exists()
            return JsonResponse({'registered': isRegistered})
        elif data['type'] == 'postSubscription':
            Subscription.objects.create(email=data['email'], name=data['name'], password=data['password'])
            return JsonResponse({'message' : 'Data berhasil disimpan!'})
        else:
            return JsonResponse({'message': 'Unknown action', 'error': True})
    else:
        return render(req, 'subscription.html')

def unsubscribe(req):
    if req.method == 'POST' and req.is_ajax():
        data = json.loads(req.body)
        if data['type'] == 'unsubscribe':
            user = Subscription.objects.all().filter(email=data['email'])[0]
            print(data)
            if (data['password'] == user.password):
                user.delete()
                return JsonResponse({'message': 'Unsubscribe successful', 'error': False})
            else:
                return JsonResponse({'message': 'Incorrect password, please try again', 'error': True})
    return JsonResponse({'message': 'Unknown action', 'error': True})

def subscriber_list(req):
    subscribers = Subscription.objects.all().values()
    return JsonResponse({'subscribers': list(subscribers)})
