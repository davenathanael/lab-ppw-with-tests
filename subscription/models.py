from django.db import models

class Subscription(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=20, default='')
    password = models.CharField(max_length=20)
