from django.urls import path
from . import views

app_name = 'subscription'

urlpatterns = [
    path('', views.index, name='index'),
    path('subscribers', views.subscriber_list, name='subscriber_list'),
    path('unsubscribe', views.unsubscribe, name='unsubscribe')
]
